function problem6(inventory) {
    const BMWOrAudiCars = [];
    for (let index = 0; index < inventory.length; index++) {
      if (inventory[index].car_make === 'BMW' || inventory[index].car_make === 'Audi') {
        BMWOrAudiCars.push(inventory[index]);
      }
    }
    return BMWOrAudiCars;
  }
  
module.exports = problem6;