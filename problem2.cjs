function problem2(inventory) {
  if (inventory.length > 0) {
    const lastCarIndex = inventory.length - 1;
    return `Last car is a ${inventory[lastCarIndex].car_make} ${inventory[lastCarIndex].car_model}`;
  }
  return 'No cars in the inventory';
}

module.exports = problem2;
