function problem5(inventory , year) {
    const olderCarsYear = [];
    for (let index = 0; index < inventory.length; index++) {
      if (inventory[index].car_year < year) {
        olderCarsYear.push(inventory[index]);
      }
    }
    return olderCarsYear;
  }

  module.exports = problem5;
  