function problem1(inventory , carId) {
  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].id === carId) {
         return `Car 33 is a ${inventory[index].car_year} ${inventory[index].car_make} ${inventory[index].car_model}`;

    }
  }
  return "Car id not found";
}

module.exports = problem1;
