function problem3(inventory) {
    let carModels = [];
    for (let index = 0; index < inventory.length; index++) {
      carModels.push(inventory[index].car_model);
    }
    carModels.sort();
    return carModels;
  }
  
module.exports = problem3;